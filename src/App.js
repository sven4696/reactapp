import React from "react";
import "./App.css";
import Login from "./Components/Login";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Translate from "./Components/Translate";

function App() {
  return (
    <Router>
      <div>
        <Switch>
        <Route path="/login" component={Login} />
        <Route path="/translate" component={Translate} />
        <Route exact path="/">
          <Redirect to="/login"></Redirect>
        </Route>
        </Switch>
      </div>
    </Router> 
  );
}

export default App;
