import React, { Component } from "react";

class Login extends Component {
  state = { name: "" };

  updateName = (event) => {
      this.setState({name: event.target.value})
      console.log(this.state.name)
  }

  storeName = (event) => {
      localStorage.setItem('name', this.state.name)
      this.props.history.push('/translate')
  }

  render() {
    return (
      <div className="App">
        <h1 className="header">Lost in Translation</h1>
        <div className="input-group mb-3 place ">
          <input
            type="text"
            input="text"
            className="form-control login text-white"
            placeholder="User Name"
            aria-label="Recipient's username"
            aria-describedby="button-addon2"
            value={this.state.name}
            onChange={this.updateName}
          />
          <div className="input-group-append">
            <button
              className="btn btn-outline-secondary login"
              type="button"
              id="login"
              onClick={this.storeName}
            >
              Login
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Login
