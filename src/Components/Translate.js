import React, { Component } from "react";

class Translate extends Component {
  state = { translateInput: "", user: localStorage.getItem("name") };

  updateInput = (event) => {
    this.setState({ name: event.target.value });
    console.log(this.state.name);
  };

  translateText = (event) => {
    let words = this.state.translateInput
    words.split('');
    let pics 
  };

  render() {
    return (
      <div>
        <h1 className="header">Welcome {this.state.user}!!</h1>

        <div className="input-group mb-3 translateInput ">
          <input
            type="text"
            input="text"
            className="form-control login text-white"
            placeholder="Enter desired translation text"
            aria-label="Recipient's username"
            aria-describedby="button-addon2"
            value={this.state.translateInput}
            onChange={this.updateInput}
          />
          <div className="input-group-append">
            <button
              className="btn btn-outline-secondary login"
              type="button"
              id="login"
              onClick={this.translateText}
            >
              Translate
            </button>
          </div>
        </div>
        <div>
          <div className="card translateCard">
            <div className="card-body">This is some text within a card body.</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Translate;
